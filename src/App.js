import React from "react";
import { useState, useEffect } from "react";
import styled from "styled-components";
import "./App.css";
import Formulario from "./components/Formulario";
import Grafico from "./components/Grafico";
import logoLipe from "./assets/logo-lipe-preto-e-laranja.png";
import logoNides from "./assets/logo-nides.png";
import logoUfrj from "./assets/logo-ufrj.png";


const PageItself = styled.html`
  display: flex;
  flex-direction: column;
`;

const MainTitle = styled.h1`
  position: relative;
  right: 50px;
  font-family: 'Economica', sans-serif;  
`;

const MainHeader = styled.header`
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  background: #d6d3f0;
  width: 100vw;
  height: 15vh;
`;

const NidesLipe = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  width: 15vw;
  height: 100%;
`;

const LogoNides = styled.img`
  width: 50%;
  height: 5vh;
  position: relative;
  right: 10px;
  transition: all 0.5s ease;
  &:hover {
    box-shadow: 2px 2px 2px 1px rgba(0, 0, 0, 0.2);
  }
`;

const LogoLipe = styled.img`
  width: 40%;
  transition: all 0.5s ease;
  &:hover {
    box-shadow: 2px 2px 2px 1px rgba(0, 0, 0, 0.2);
  }
`;

const LogoUfrj = styled.img`
  width: 8vw;
  height: 8vh;
  transition: all 0.5s ease;
  &:hover {
    box-shadow: 2px 2px 2px 1px rgba(0, 0, 0, 0.2);
  }
`;

function App() {
  const [dados, setDados] = useState({"measurements":[]});

  useEffect(() => {
    console.log(dados);
  }, [dados]);

  return (
    <PageItself>
      <MainHeader>
        <NidesLipe>
          <a
            href="http://nides.ufrj.br/index.php/o-lipe"
            target="_blank"
            rel="noreferrer noopener"
          >
            <LogoLipe src={logoLipe} />
          </a>
          <a href="http://nides.ufrj.br/index.php"
            target="_blank"
            rel="noreferrer noopener">
          <LogoNides src={logoNides} />
          </a>
        </NidesLipe>
        <MainTitle>MEDIDOR DE MARÉ</MainTitle>
        <a href="https://ufrj.br/" target="_blank" rel="noreferrer noopener">
          <LogoUfrj src={logoUfrj} />
        </a>
      </MainHeader>
      <Formulario updateState={setDados}/>
      <Grafico data={dados} />
    </PageItself>
  );
}

export default App;
