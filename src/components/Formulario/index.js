import React from 'react';
import { useForm } from 'react-hook-form';
import axios from 'axios';

export default function Formulario(props) {
  const { register, handleSubmit, formState: { errors } } = useForm();
  console.log(errors);

  async function onSubmit(inputs) {
    const body = {...inputs, "source":{"devices": inputs.devices.split()}, "startTime":parseInt(inputs.startTime), "endTime":parseInt(inputs.endTime)};
    
    axios.post("http://localhost:3000/getMeasurements", body)
      .then(function(response) {
        console.log(response.data);
        props.updateState(response.data);
      })
      .catch(function(error) {
        console.error(error);
      })
  }

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
          <input type="text" placeholder="Tipo de consulta" {...register("type", { required: true })} />
          <input type="text" placeholder="Dispositivos (separados por vírgulas)" {...register("devices", { required: true, maxLength: 100 })} />
          <input type="number" placeholder="Tempo de início" {...register("startTime", { required: true })} />
          <input type="number" placeholder="Tempo de fim" {...register("endTime", { required: true })} />

          <input type="submit" />
        </form>
    );
}
