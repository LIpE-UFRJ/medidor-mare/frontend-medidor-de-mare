import { LineChart, Line, CartesianGrid, XAxis, YAxis } from 'recharts';

export default function Grafico(props) {
  return (
    <LineChart width={600} height={300} data={[...props.data.measurements]}>
      <XAxis dataKey="timestamp" />
      <YAxis/>
      <CartesianGrid stroke="#ccc" />
      <Line type="monotone" dataKey="value" stroke="#8884d8" />
    </LineChart>
  );
}
