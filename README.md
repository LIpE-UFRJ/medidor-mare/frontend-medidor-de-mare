# Frontend - Medidor de Maré

Frontend do projeto "Medidor de Maré", onde é possível fazer consultas e visualizar os dados coletados através de gráficos.

## Dependências

O sistema tem como dependências no sistema:

* nodejs

Após instalação, é necessário instalar os node modules com o comando:
npm install

## Desenvolvimento

No diretório do projeto, você pode rodar:

### `npm start`

Roda o aplicativo em modo de desenvolvimento. Acessar [http://localhost:3000](http://localhost:3000) para visualizar no navegador.
